package com.codeon.payment.controller;

import com.codeon.payment.entity.Payment;
import com.codeon.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService service;

    @GetMapping("/")
    public String welcome() { return "Payment Service Active!!"; }

    @PostMapping("/doPayment")
    public Payment doPayment(@RequestBody Payment payment) {
        return service.doPayment(payment);
    }

    @GetMapping("/order/{id}")
    public List<Payment> getPaymentByOrderId(@PathVariable String id) {
        log.debug("getPaymentByOrderId called");
        return service.getPaymentByOrderId(id);
    }
}
