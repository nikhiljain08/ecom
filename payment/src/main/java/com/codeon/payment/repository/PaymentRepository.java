package com.codeon.payment.repository;

import com.codeon.payment.entity.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentRepository  extends MongoRepository<Payment, Integer> {
    List<Payment> findAllByOrderId(String orderId);
}
