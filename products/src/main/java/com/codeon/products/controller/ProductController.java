package com.codeon.products.controller;

import com.codeon.products.entity.Product;
import com.codeon.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping("/")
    public String welcome() {
        return "Product service is active!!";
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody Product product) {
        return service.addProduct(product);
    }

    @GetMapping("/getProduct")
    public Optional<Product> getProduct(@RequestParam String id) {
        return service.getProduct(id);
    }

    @GetMapping("/all")
    public List<Product> getAllProduct() {
        return service.getAllProduct();
    }
}
