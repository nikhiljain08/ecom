package com.codeon.products.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailRequest {

    @Id
    private String _id;
    private String to;
    private String from;
    private String title;
    private String message;
    private Timestamp created;
    private Timestamp scheduled;
}
