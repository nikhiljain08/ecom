package com.codeon.products.service;

import com.codeon.products.entity.Product;
import com.codeon.products.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public Product addProduct(Product product) {
        return repository.save(product);
    }

    public Optional<Product> getProduct(String id) {
        return repository.findById(id);
    }

    public List<Product> getAllProduct() {
        return repository.findAll();
    }
}
