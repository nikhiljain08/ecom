package com.codeon.user.repository;

import com.codeon.user.entity.UserAudit;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserAuditRepository extends MongoRepository<UserAudit, String> {
}
