package com.codeon.user.repository;

import com.codeon.user.entity.RefreshToken;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RefreshTokenRepository extends MongoRepository<RefreshToken, Long> {
    RefreshToken findByToken(String token);
    void deleteByEmail(String email);
}
