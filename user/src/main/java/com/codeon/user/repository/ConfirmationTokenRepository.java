package com.codeon.user.repository;

import com.codeon.user.entity.ConfirmationToken;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ConfirmationTokenRepository extends MongoRepository<ConfirmationToken, String> {
    ConfirmationToken findByToken(String token);
    List<ConfirmationToken> findAllByUserId(String id);
}
