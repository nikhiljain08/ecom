package com.codeon.user.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailRequest {

    private String to;
    private Map<String, Object> template;
    private LocalDateTime created;
    private LocalDateTime scheduled;
    private LocalDateTime sendAt;
}
