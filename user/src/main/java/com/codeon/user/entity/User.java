package com.codeon.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "USER_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String _id;
    private String name;
    private String email;
    @JsonIgnore
    private String password;
    private List<String> roles;
    @Builder.Default
    private boolean valid = false;
    @Builder.Default
    private boolean active = false;
    @Builder.Default
    private boolean lock =  false;
    private LocalDateTime createdAt;
    private LocalDateTime lastAccessedTime;
}
