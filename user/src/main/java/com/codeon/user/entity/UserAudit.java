package com.codeon.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "USER_AUDIT_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAudit {

    @Id
    private String _id;
    private String userId;
    private String ipAddress;
    private String agent;
    @Builder.Default
    private LocalDateTime lastLoggedIn = LocalDateTime.now();

}
