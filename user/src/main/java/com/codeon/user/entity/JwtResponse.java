package com.codeon.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {

    @Id
    private String _id;
    private String name;
    private String email;
    private List<String> roles;
    private boolean valid;
    private boolean active;
    @Builder.Default
    private String type = "Bearer";
    private String token;
    private String refreshToken;
    private boolean lock;
    private LocalDateTime createdAt;
    private LocalDateTime lastAccessedTime;

}
