package com.codeon.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomErrorResponse {

    @Builder.Default
    private LocalDateTime timestamp = LocalDateTime.now();
    private HttpStatus status;
    private String error;
    private String message;
    private String path;

}
