package com.codeon.user.controller;

import com.codeon.user.entity.*;
import com.codeon.user.service.ConfirmationTokenService;
import com.codeon.user.service.RefreshTokenService;
import com.codeon.user.service.UserAuditService;
import com.codeon.user.service.UserService;
import com.codeon.user.util.EmailValidator;
import com.codeon.user.util.HttpReqRespUtils;
import com.codeon.user.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserService service;

    @Autowired
    private UserAuditService userAuditService;

    @Autowired
    private ConfirmationTokenService tokenService;

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @GetMapping("/")
    public String welcome() { return "User Service Active!!"; }

    @PostMapping(value = "/register")
    public ResponseEntity<?> addUser(@RequestBody UserRegister userRegister) {
        log.debug("addUser is called");
        if(userRegister.getEmail() == null || userRegister.getPassword() == null
                || userRegister.getEmail().isEmpty() || userRegister.getPassword().isEmpty()) {
            log.debug("Invalid data!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("INVALID DATA")
                    .message("Invalid data!")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        }

        if(service.isUserExistsByEmail(userRegister.getEmail())) {
            log.debug("User already registered!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.NOT_FOUND)
                    .error("User already registered!")
                    .message("User already registered!")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        } else {
            User user = service.addUser(userRegister);
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.NOT_FOUND)
                    .error("Error occurred while registering!")
                    .message(new InvalidParameterException().getMessage())
                    .path(request.getRequestURI())
                    .build();
            if(user == null)
                log.debug("Error occurred while registering!");
            return ResponseEntity.ok(Objects.requireNonNullElse(user, response));
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getUser(@PathVariable String id) {
        log.debug("getUser is called");
        Optional<User> user = service.getUser(id);
        if(user.isEmpty()) {
            log.debug("User not found!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.NOT_FOUND)
                    .error("NOT FOUND")
                    .message("User not found")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.ok(user.get());
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> authenticateUser(@RequestBody AuthRequest authRequest) {
        log.debug("authenticateUser is called");

        if(authRequest.getEmail() == null || authRequest.getPassword() == null
                || authRequest.getEmail().isEmpty() || authRequest.getPassword().isEmpty()) {
            log.debug("Invalid data!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("INVALID DATA")
                    .message("Invalid data!")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        }

        String email = authRequest.getEmail();
        boolean isValidEmail = emailValidator.test(email);
        if (isValidEmail) {
            if(!service.isUserExistsByEmail(email)) {
                log.debug("User not found!");
                CustomErrorResponse response = CustomErrorResponse.builder()
                        .status(HttpStatus.NOT_FOUND)
                        .error("NOT FOUND")
                        .message("User not found")
                        .path(request.getRequestURI())
                        .build();
                return ResponseEntity.ok(response);
            }
        } else {
            log.debug("Invalid email!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("INVALID DATA")
                    .message("Invalid email!")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        }

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email,
                            authRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            String jwt = jwtTokenUtil.generateToken(userDetails.getUsername());
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.get_id());

            UserAudit audit = UserAudit.builder()
                    .userId(userDetails.get_id())
                    .ipAddress(HttpReqRespUtils.getIPFromRequest(request))
                    .agent(HttpReqRespUtils.getAgent(request))
                    .build();
            userAuditService.saveUserData(audit);

            Optional<User> user = service.getUser(userDetails.get_id());
            LocalDateTime lastAccessedTime = LocalDateTime.now();
            if(user.isPresent()) {
                lastAccessedTime = user.get().getLastAccessedTime();
            }

            JwtResponse jwtResponse = JwtResponse.builder()
                    ._id(userDetails.get_id())
                    .name(userDetails.getName())
                    .email(userDetails.getEmail())
                    .roles(roles)
                    .valid(userDetails.isValid())
                    .active(userDetails.isActive())
                    .lock(userDetails.isLock())
                    .token(jwt)
                    .refreshToken(refreshToken.getToken())
                    .createdAt(userDetails.getCreatedAt())
                    .lastAccessedTime(lastAccessedTime)
                    .build();

            if(userDetails.getLastAccessedTime().isBefore(lastAccessedTime.minusDays(7))) {
                service.sendLoggedInMail(userDetails.getEmail(), userDetails.getName());
            }
            return ResponseEntity.ok(jwtResponse);
        }  catch (DisabledException e) {
            log.error("User Disabled! -> " + e.getMessage());
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("User Disabled")
                    .message(e.getMessage())
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        } catch (BadCredentialsException e) {
            log.error("Invalid credentials! -> " + e.getMessage());
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("Invalid credentials")
                    .message(e.getMessage())
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            log.error("Unknown Exception -> " + e.getMessage());
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .error("Unknown Exception")
                    .message(e.getMessage())
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshToken(@RequestBody TokenRefreshRequest tokenRefreshRequest) {
        log.debug("refreshToken is called");
        String requestRefreshToken = tokenRefreshRequest.getRefreshToken();
        RefreshToken refreshToken = refreshTokenService.findByToken(requestRefreshToken);
        if(refreshToken!=null && refreshTokenService.verifyExpiration(refreshToken)!=null) {
            String token = jwtTokenUtil.generateToken(refreshToken.getEmail());
            return ResponseEntity.ok(TokenRefreshResponse.builder().token(token).refreshToken(requestRefreshToken).build());
        }
        log.debug("Invalid token");
        CustomErrorResponse response = CustomErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST)
                .error("INVALID TOKEN")
                .message("Invalid token")
                .path(request.getRequestURI())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/confirm")
    public ResponseEntity<?> confirm(@RequestParam("token") String token) {
        log.debug("confirm is called");
        ConfirmationToken confirmationToken = tokenService.findByToken(token);
        if(confirmationToken == null) {
            log.debug("Token not found!");
            CustomErrorResponse response = CustomErrorResponse.builder()
                    .status(HttpStatus.NOT_FOUND)
                    .error("NOT FOUND")
                    .message("Token not found!")
                    .path(request.getRequestURI())
                    .build();
            return ResponseEntity.ok(response);
        } else {
            if (tokenService.isTokenConfirmed(confirmationToken)) {
                log.debug("Email already confirmed");
                CustomErrorResponse response = CustomErrorResponse.builder()
                        .status(HttpStatus.BAD_REQUEST)
                        .error("ALREADY CONFIRMED")
                        .message("Email already confirmed")
                        .path(request.getRequestURI())
                        .build();
                return ResponseEntity.ok(response);
            }

            if (tokenService.isTokenExpired(confirmationToken)) {
                log.debug("Token expired");
                CustomErrorResponse response = CustomErrorResponse.builder()
                        .status(HttpStatus.BAD_REQUEST)
                        .error("EXPIRED")
                        .message("Token expired")
                        .path(request.getRequestURI())
                        .build();
                return ResponseEntity.ok(response);
            }
            return ResponseEntity.ok(tokenService.confirmToken(confirmationToken));
        }
    }

    @GetMapping("/resend")
    public ResponseEntity<?> resendMail(@RequestParam("id") String id) {
        log.debug("resendMail is called");
        ResponseEntity<?> response = ResponseEntity.ok("Sent");
        CustomErrorResponse customErrorResponse;
        switch (service.resendVerifyMail(id)) {
            case "Already verified" :
                log.debug("Already verified");
                customErrorResponse = CustomErrorResponse.builder()
                        .status(HttpStatus.BAD_REQUEST)
                        .error("VERIFIED")
                        .message("Already verified")
                        .path(request.getRequestURI())
                        .build();
                response = ResponseEntity.ok(customErrorResponse);
                break;
            case "User not found" :
                log.debug("User not found");
                customErrorResponse = CustomErrorResponse.builder()
                        .status(HttpStatus.NOT_FOUND)
                        .error("NOT FOUND")
                        .message("User not found")
                        .path(request.getRequestURI())
                        .build();
                response = ResponseEntity.ok(customErrorResponse);
                break;
        }
        return response;
    }
}
