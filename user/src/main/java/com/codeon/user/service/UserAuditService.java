package com.codeon.user.service;

import com.codeon.user.entity.UserAudit;
import com.codeon.user.repository.UserAuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAuditService {

    @Autowired
    UserAuditRepository repository;

    public void saveUserData(UserAudit userAudit) {
        repository.save(userAudit);
    }
}
