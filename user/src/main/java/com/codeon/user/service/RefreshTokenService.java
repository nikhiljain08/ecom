package com.codeon.user.service;

import com.codeon.user.entity.RefreshToken;
import com.codeon.user.repository.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Value("${app.jwt.jwtRefreshExpirationMs}")
    private Long refreshTokenDurationMs;

    @Autowired
    private RefreshTokenRepository repository;

    public RefreshToken findByToken(String token) {
        return repository.findByToken(token);
    }

    public RefreshToken createRefreshToken(String email) {
        repository.deleteByEmail(email);
        RefreshToken refreshToken = RefreshToken.builder()
                .email(email)
                .expiryDate(LocalDateTime.now().plusSeconds(refreshTokenDurationMs/1000))
                .token(UUID.randomUUID().toString())
                .build();
        return  repository.save(refreshToken);
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(LocalDateTime.now()) < 0) {
            repository.delete(token);
            return null;
        }
        return token;
    }

    public void deleteByEmailId(String email) {
        repository.deleteByEmail(email);
    }


}
