package com.codeon.user.service;

import com.codeon.user.entity.User;
import com.codeon.user.entity.ConfirmationToken;
import com.codeon.user.repository.ConfirmationTokenRepository;
import com.codeon.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ConfirmationTokenService {

    @Autowired
    ConfirmationTokenRepository repository;

    @Autowired
    UserRepository userRepository;

    public ConfirmationToken findByToken(String token) {
        return repository.findByToken(token);
    }

    public String confirmToken(ConfirmationToken confirmationToken) {
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        repository.save(confirmationToken);
        Optional<User> user = userRepository.findById(confirmationToken.getUserId());
        if (user.isPresent()) {
            User updated_user = user.get();
            updated_user.setActive(true);
            updated_user.setValid(true);
            userRepository.save(updated_user);
        }
        return "CONFIRMED";
    }

    public boolean isTokenConfirmed(ConfirmationToken confirmationToken) {
        return confirmationToken.getConfirmedAt() != null;
    }

    public boolean isTokenExpired(ConfirmationToken confirmationToken) {
        LocalDateTime expiredAt = confirmationToken.getExpiresAt();
        return expiredAt.isBefore(LocalDateTime.now());
    }

    public ConfirmationToken validate(String id) {
        List<ConfirmationToken> tokenList = repository.findAllByUserId(id);
        for(ConfirmationToken token : tokenList) {
            if(!isTokenExpired(token) && !isTokenConfirmed(token)) {
                return token;
            }
        }
        return null;
    }

    public void saveConfirmationToken(ConfirmationToken confirmationToken) {
        repository.save(confirmationToken);
    }

    public void expireAll(String id) {
        List<ConfirmationToken> tokenList = repository.findAllByUserId(id);
        for(ConfirmationToken token : tokenList) {
            if(!isTokenExpired(token) && !isTokenConfirmed(token)) {
                token.setExpiresAt(LocalDateTime.now());
                repository.save(token);
            }
        }
    }
}

