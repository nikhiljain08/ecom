package com.codeon.user.service;

import com.codeon.user.common.MailRequest;
import com.codeon.user.entity.ConfirmationToken;
import com.codeon.user.entity.User;
import com.codeon.user.entity.UserDetailsImpl;
import com.codeon.user.entity.UserRegister;
import com.codeon.user.repository.UserRepository;
import com.codeon.user.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository repository;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    private DiscoveryClient client;

    @Autowired
    private KafkaTemplate<String, MailRequest> kafkaTemplate;

    public User addUser(UserRegister userRegister) {
        String url = getInstanceURL(client.getInstances("API-GATEWAY").get(0));
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = User.builder()
                .name(userRegister.getName())
                .email(userRegister.getEmail())
                .password(encoder.encode(userRegister.getPassword()))
                .roles(List.of("ROLE_USER"))
                .lock(false)
                .createdAt(LocalDateTime.now())
                .lastAccessedTime(LocalDateTime.now())
                .build();

        user = repository.save(user);
        ConfirmationToken confirmationToken = generateConfirmToken(user.get_id());
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        this.sendSignUpMail(user, confirmationToken, url);
        return user;
    }

    public User saveUser(User user) {
        return repository.save(user);
    }

    public Optional<User> getUser(String id) {
        Optional<User> user = repository.findById(id);
        if(user.isPresent()) {
            user.get().setLastAccessedTime(LocalDateTime.now());
            saveUser(user.get());
        }
        return user;
    }

    public boolean isUserExistsByEmail(String email) {
        return this.getUserByEmail(email)!=null;
    }

    public User getUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = this.getUserByEmail(email);
        return UserDetailsImpl.build(user);
    }

    public ConfirmationToken generateConfirmToken(String id) {
        String token = UUID.randomUUID().toString();

        return ConfirmationToken.builder()
            .userId(id)
            .token(token)
            .createdAt(LocalDateTime.now())
            .expiresAt(LocalDateTime.now().plusMinutes(15))
            .build();
    }

    public void sendSignUpMail(User user, ConfirmationToken confirmationToken, String url) {
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("name", user.getName());
        templateData.put("token", String.format("%s/user/confirm?token=%s", url, confirmationToken.getToken()));

        Map<String, Object> mailTemplate = new HashMap<>();
        mailTemplate.put("name", "signup");
        mailTemplate.put("data", templateData);

        MailRequest mailRequest = MailRequest.builder()
                .to(user.getEmail())
                .template(mailTemplate)
                .created(LocalDateTime.now())
                .scheduled(LocalDateTime.now().plusMinutes(15))
                .build();
        kafkaTemplate.send("EMAIL_SIGNUP", mailRequest);
    }

    public void sendLoggedInMail(String email, String name) {
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("name", name);

        Map<String, Object> mailTemplate = new HashMap<>();
        mailTemplate.put("name", "loggedIn");
        mailTemplate.put("data", templateData);

        MailRequest mailRequest = MailRequest.builder()
                .to(email)
                .template(mailTemplate)
                .created(LocalDateTime.now())
                .scheduled(LocalDateTime.now().plusMinutes(15))
                .build();
        kafkaTemplate.send("EMAIL_LOGGED_IN", mailRequest);
    }

    public String resendVerifyMail(String id) {
        Optional<User> user = repository.findById(id);
        if(user.isPresent()) {
            if(!user.get().isValid()) {
                String url = getInstanceURL(client.getInstances("API-GATEWAY").get(0));
                confirmationTokenService.expireAll(id);
                ConfirmationToken confirmationToken = generateConfirmToken(id);
                confirmationTokenService.saveConfirmationToken(confirmationToken);
                this.sendSignUpMail(user.get(), confirmationToken, url);
                return "Sent";
            } else {
                return "Already verified";
            }
        } else {
            return "User not found";
        }
    }

    private String getInstanceURL(ServiceInstance payment_serviceInstance) {
        if(!payment_serviceInstance.getHost().contains("localhost")) {
            URI uri;
            try {
                uri = new URI("https", payment_serviceInstance.getUri().getHost(),
                        payment_serviceInstance.getUri().getPath(), payment_serviceInstance.getUri().getFragment());
                return uri.toString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return payment_serviceInstance.getUri().toString();
    }
}
