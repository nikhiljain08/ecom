package com.codeon.order.service;

import com.codeon.order.common.*;
import com.codeon.order.entity.Order;
import com.codeon.order.entity.OrderProduct;
import com.codeon.order.repository.OrderRepository;
import com.codeon.order.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.*;

@Slf4j
@Service
public class OrderService {

    private static final String PAYMENT_ENDPOINT_URL = "/payment/doPayment";
    private static final String PRODUCT_ENDPOINT_URL = "/product/getProduct";
    private static final String USER_ENDPOINT_URL = "/user/";

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RestTemplate template;

    @Autowired
    private DiscoveryClient client;

    @Autowired
    private KafkaTemplate<String, EmailRequest> kafkaTemplate;

    @Value(value = "${kafkaTopics.email}")
    private String email_topic;


    public OrderResponse saveOrder(String token, OrderRequest request) {
//        payment.setAmount(saved_order.getTotalAmount());
//        String pay_url = getInstanceURL(client.getInstances("PAYMENT-SERVICE").get(0));
//        Payment paymentResponse = template.postForObject(pay_url + PAYMENT_ENDPOINT_URL, payment, Payment.class);
//        String response = paymentResponse.getPaymentStatus().equals("success") ?
//                "payment processing successful and order placed" : "there is a failure in payment api , order added to cart";

        String user_url = getInstanceURL(client.getInstances("USER-SERVICE").get(0));
        UriComponents userBuilder = UriComponentsBuilder.fromHttpUrl(user_url + USER_ENDPOINT_URL + request.getOrder().getUserId()).build();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token.split(" ")[1]);
        HttpEntity<Void> entity = new HttpEntity<>(headers);
        ResponseEntity<User> userResponse = template.exchange(userBuilder.toString(), HttpMethod.GET, entity, User.class);
        log.error("User -> " + userResponse.getBody());

        List<Product> products = new ArrayList<>();
        for(OrderProduct orderProduct : request.getOrderProducts()) {
            String prod_url = getInstanceURL(client.getInstances("PRODUCT-SERVICE").get(0));
            UriComponents builder = UriComponentsBuilder.fromHttpUrl(prod_url + PRODUCT_ENDPOINT_URL)
                    .queryParam("id",orderProduct.getProductId()).build();
            Product productResponse = template.getForObject(builder.toString(), Product.class);
            Product product = new Product();
            orderProduct = productRepository.save(orderProduct);
            product.set_id(orderProduct.get_id());
            product.setQty(orderProduct.getQty());
            product.setPrice(orderProduct.getPrice());
            product.setQty_type(productResponse.getQty_type());
            product.setDescription(productResponse.getDescription());
            product.setName(productResponse.getName());
            product.setImg_url(productResponse.getImg_url());
            products.add(product);
        }

        if(products.isEmpty() ) {
            return null;
        }

        Order order = orderRepository.save(request.getOrder());

        EmailRequest emailRequest = EmailRequest.builder()
                .to(userResponse.getBody().getEmail())
                .from("jibbymailer@gmail.com")
                .title("Your recent purchase!")
                .message(String.format("Hi %s, \nThanks for your recent purchase!", userResponse.getBody().getName()) )
                .created(new Timestamp(System.currentTimeMillis()))
                .scheduled(new Timestamp(System.currentTimeMillis() + 60*10000))
                .build();
        kafkaTemplate.send(email_topic, emailRequest);

        return new OrderResponse(order, products, order.getTotalAmount(), "", "Order created only!");
    }

    private String getInstanceURL(ServiceInstance payment_serviceInstance) {
        if(!payment_serviceInstance.getHost().contains("localhost")) {
            URI uri;
            try {
                uri = new URI("https", payment_serviceInstance.getUri().getHost(),
                        payment_serviceInstance.getUri().getPath(), payment_serviceInstance.getUri().getFragment());
                return uri.toString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return payment_serviceInstance.getUri().toString();
    }
}
