package com.codeon.order.repository;

import com.codeon.order.entity.OrderProduct;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<OrderProduct, String> {
}
