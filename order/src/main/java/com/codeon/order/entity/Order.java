package com.codeon.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ORDER_TB")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    @Id
    private String _id;
    private double offerAmount;
    private double deliveryFee;
    private String payMode;
    private double totalAmount;
    private String userId;
    private String addressId;
}
