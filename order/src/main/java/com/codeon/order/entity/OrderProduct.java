package com.codeon.order.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ORDER_PRODUCT_TB")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderProduct {

    @Id
    private String _id;
    private int qty;
    private double price;
    private String orderId;
    private String productId;
}
