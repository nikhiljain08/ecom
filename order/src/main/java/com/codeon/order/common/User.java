package com.codeon.order.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String _id;
    private String name;
    private String email;
    @JsonIgnore
    private String password;
    private List<String> roles;
    @Builder.Default
    private boolean valid = false;
    @Builder.Default
    private boolean active = false;

}
