package com.codeon.order.common;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    private String _id;
    private String name;
    private String description;
    private String img_url;
    private int qty;
    private double price;
    private String qty_type;
}
