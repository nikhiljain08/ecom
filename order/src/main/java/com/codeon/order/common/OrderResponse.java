package com.codeon.order.common;

import com.codeon.order.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {

    private Order order;
    private List<Product> products;
    private double amount;
    private String transactionId;
    private String message;
}
