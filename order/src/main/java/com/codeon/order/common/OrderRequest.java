package com.codeon.order.common;

import com.codeon.order.entity.Order;
import com.codeon.order.entity.OrderProduct;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {

    private Order order;
    private List<OrderProduct> orderProducts;
}
