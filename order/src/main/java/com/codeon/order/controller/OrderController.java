package com.codeon.order.controller;

import com.codeon.order.common.OrderRequest;
import com.codeon.order.common.OrderResponse;
import com.codeon.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService service;

    @GetMapping("/")
    public String welcome() {
        return "Order Service Active!";
    }

    @PostMapping("/bookOrder")
    public OrderResponse createOrder(@RequestHeader("Authorization") String token, @RequestBody OrderRequest request) {
        log.debug("createOrder called");
        return service.saveOrder(token, request);
    }
}
