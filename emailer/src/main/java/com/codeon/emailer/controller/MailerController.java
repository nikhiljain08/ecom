package com.codeon.emailer.controller;

import com.codeon.emailer.entity.MailRequest;
import com.codeon.emailer.service.MailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mail")
public class MailerController {

    @Autowired
    MailerService service;

    @GetMapping("/")
    public String welcome() {
        return "Mailer service is running!";
    }

    @PostMapping("/send")
    public String sendMail(@RequestBody MailRequest payload) {
        return service.sendTemplatePDFMail(payload);
    }
}
