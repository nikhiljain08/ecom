package com.codeon.emailer.util;

import java.util.Map;

public class Util {

    public static String replaceParams(Map<String, Object> hashMap, String template) {
        return hashMap.entrySet().stream().reduce(template, (s, e) -> s.replace("{{" + e.getKey() + "}}", String.valueOf(e.getValue())),
                (s, s2) -> s);
    }
}
