package com.codeon.emailer.service;

import com.codeon.emailer.entity.MailRequest;
import com.codeon.emailer.entity.MailTemplate;
import com.codeon.emailer.entity.PDFTemplate;
import com.codeon.emailer.repository.MailerRepository;
import com.codeon.emailer.repository.PDFTemplateRepository;
import com.codeon.emailer.repository.TemplateRepository;
import com.codeon.emailer.util.Util;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.styledxmlparser.IXmlParser;
import com.itextpdf.styledxmlparser.jsoup.nodes.Element;
import com.itextpdf.styledxmlparser.node.IDocumentNode;
import com.itextpdf.styledxmlparser.node.impl.jsoup.JsoupHtmlParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.print.Doc;
import java.io.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.in;

@Slf4j
@Service
public class MailerService {

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    MailerRepository mailerRepository;

    @Autowired
    TemplateRepository templateRepository;

    @Autowired
    PDFTemplateRepository pdfTemplateRepository;

    @SuppressWarnings("unchecked")
    public void sendTemplateMail(MailRequest payload) {
        Map<String, Object> template = payload.getTemplate();
        String template_name = (String) template.get("name");
        Map<String, Object> data = (Map<String, Object>) template.get("data");
        MailTemplate mailTemplate = templateRepository.findByName(template_name);

        String subject = mailTemplate.getSubject();
        String html = Util.replaceParams(data, mailTemplate.getHtml());

        try {
            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            mailMessage.setSubject(subject, "UTF-8");
            MimeMessageHelper msg = new MimeMessageHelper(mailMessage, true, "UTF-8");
            msg.setTo(payload.getTo());
            msg.setText(html, true);

            payload.setSendAt(LocalDateTime.now());
            mailerRepository.save(payload);

            javaMailSender.send(mailMessage);
            log.debug("Mail send successfully! : " + msg);
        } catch (MailException exception) {
            log.error("Error in sending mail : ", exception);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public String sendTemplatePDFMail(MailRequest payload) {
        Map<String, Object> template = payload.getTemplate();
        String template_name = (String) template.get("name");
        Map<String, Object> data = (Map<String, Object>) template.get("data");
        MailTemplate mailTemplate = templateRepository.findByName(template_name);

        boolean isAttachment = mailTemplate.isAttachment();
        String subject = mailTemplate.getSubject();
        String html = Util.replaceParams(data, mailTemplate.getHtml());

        try {
            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            mailMessage.setSubject(subject, "UTF-8");
            MimeMessageHelper msg = new MimeMessageHelper(mailMessage, true, "UTF-8");
            msg.setTo(payload.getTo());
            msg.setText(html, true);

            if(isAttachment && mailTemplate.getPdfTemplates().length > 0) {
                List<PDFTemplate> pdfTemplateList = pdfTemplateRepository.findAllByNames(mailTemplate.getPdfTemplates());
                pdfTemplateList.forEach(pdfTemplate -> {
                    try {

                        String pdfHtml = Util.replaceParams(data, pdfTemplate.getData());
                        final ByteArrayOutputStream stream = createInMemoryDocument(pdfHtml);
                        final InputStreamSource attachment = new ByteArrayResource(stream.toByteArray());
                        msg.addAttachment(pdfTemplate.getFilename(), attachment, pdfTemplate.getFiletype());
                    } catch (MessagingException | IOException e) {
                        e.printStackTrace();
                    }
                });
            }

            payload.setSendAt(LocalDateTime.now());
            mailerRepository.save(payload);
            javaMailSender.send(mailMessage);
            log.debug("Mail send successfully! : " + msg);
            return "Mail send successfully!";
        } catch (MailException exception) {
            log.error("Error in sending mail : ", exception);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return "Mail send unsuccessfully!";
    }

    private ByteArrayOutputStream createInMemoryDocument(String documentBody) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ConverterProperties properties = new ConverterProperties();
        PdfDocument pdf = new PdfDocument(new PdfWriter(outputStream));
        Document document =
                HtmlConverter.convertToDocument(documentBody, pdf, properties);
        document.close();
        return outputStream;
    }
}
