package com.codeon.emailer.repository;

import com.codeon.emailer.entity.MailTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemplateRepository extends MongoRepository<MailTemplate, String> {
    MailTemplate findByName(String template_name);
}
