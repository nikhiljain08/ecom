package com.codeon.emailer.repository;

import com.codeon.emailer.entity.PDFTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PDFTemplateRepository extends MongoRepository<PDFTemplate, String> {
    @Query(value = "{ name: { $in: ?0 } }")
    List<PDFTemplate> findAllByNames(String[] pdfTemplates);
}
