package com.codeon.emailer.repository;

import com.codeon.emailer.entity.MailRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MailerRepository extends MongoRepository<MailRequest, String> {
}
