package com.codeon.emailer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PDF_TEMPLATE_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PDFTemplate {

    @Id
    private String _id;
    private String name;
    private String filename;
    @Builder.Default
    private String filetype = "application/pdf";
    private String data;
}
