package com.codeon.emailer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "MAIL_TEMPLATE_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailTemplate {

    @Id
    private String _id;
    private String name;
    private String html;
    private String subject;
    private boolean isAttachment;
    private String[] pdfTemplates;
}
