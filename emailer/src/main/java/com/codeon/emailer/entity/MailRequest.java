package com.codeon.emailer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Map;

@Document(collection = "MAIL_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailRequest {

    @Id
    private String _id;
    private String to;
    private Map<String, Object> template;
    private LocalDateTime created;
    private LocalDateTime scheduled;
    private LocalDateTime sendAt;
}
