package com.codeon.emailer.config;

import com.codeon.emailer.entity.MailRequest;
import com.codeon.emailer.service.MailerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MailReceiver {

    @Autowired
    MailerService service;

    @KafkaListener(topics = "EMAIL_SIGNUP", containerFactory = "MailRequestFactory")
    public void receiveSignUpEmail(@Payload MailRequest payload) {
        service.sendTemplatePDFMail(payload);
    }

    @KafkaListener(topics = "EMAIL_LOGGED_IN", containerFactory = "MailRequestFactory")
    public void receiveLoggedInEmail(@Payload MailRequest payload) {
        service.sendTemplatePDFMail(payload);
    }

    @KafkaListener(topics = "EMAIL_ORDER", containerFactory = "MailRequestFactory")
    public void receiveOrderEmail(@Payload MailRequest payload) {
        service.sendTemplateMail(payload);
    }
}
