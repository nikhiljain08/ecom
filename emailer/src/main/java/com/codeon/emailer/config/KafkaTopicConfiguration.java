package com.codeon.emailer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfiguration {

    @Bean
    public NewTopic createSignUpEmailTopic() {
        return TopicBuilder.name("EMAIL_SIGNUP")
                .partitions(1)
                .replicas(1)
                .compact()
                .build();
    }

    @Bean
    public NewTopic createLoggedInEmailTopic() {
        return TopicBuilder.name("EMAIL_LOGGED_IN")
                .partitions(1)
                .replicas(1)
                .compact()
                .build();
    }

    @Bean
    public NewTopic createOrderEmailTopic() {
        return TopicBuilder.name("EMAIL_ORDER")
                .partitions(1)
                .replicas(1)
                .compact()
                .build();
    }
}
