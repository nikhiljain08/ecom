package com.codeon.emailer.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfiguration {

    @Value(value = "${spring.mail.host}")
    String host;

    @Value(value = "${spring.mail.port}")
    int port;

    @Value(value = "${spring.mail.username}")
    String username;

    @Value(value = "${spring.mail.password}")
    String password;

    @Value(value = "${spring.mail.properties.mail.smtp.auth}")
    boolean auth;

    @Value(value = "${spring.mail.properties.mail.smtp.connectiontimeout}")
    int connectiontimeout;

    @Value(value = "${spring.mail.properties.mail.smtp.timeout}")
    int timeout;

    @Value(value = "${spring.mail.properties.mail.smtp.writetimeout}")
    int writetimeout;

    @Value(value = "${spring.mail.properties.mail.smtp.starttls.enable}")
    boolean starttls;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);

        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.connectiontimeout", connectiontimeout);
        props.put("mail.smtp.timeout", timeout);
        props.put("mail.smtp.writetimeout", writetimeout);
        props.put("mail.smtp.starttls.enable", starttls);

        return mailSender;
    }
}
